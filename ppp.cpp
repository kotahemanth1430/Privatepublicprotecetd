#include<iostream>
using namespace std;
class Room{
    public:
    int a=20;
    private:
    int b=30;
    protected:
    int c=10;

};
class Room1:public Room{
    void display(){
        cout<<"The value of a is : "<<a<<endl;
    }
};
class Room2:private Room{
    void display2(){
        cout<<"the value of b is "<<b<<endl;
    }
};
class Room3:protected Room{
    void display3()
    {
        cout<<"the value of c is : "<<c<<endl;
    }
};
int main()
{
    Room1 obj1;
    obj1.display();
    Room2 obj2;
    obj2.display2();
    Room3 obj3;
    obj3.display3();
}